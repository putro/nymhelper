#!/usr/bin/python3
#
# nymhelper.py -- script to help creating nyms
# version 0.3
#
# Copyright (C) 2011-2017 putro <putro@autistici.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.

import os, sys
import re, string
import gnupg
import getpass
import subprocess
import traceback
import configparser
import random

class bcolors:
    """
    Define colors for outputs
    """
    MAGENTA = '\033[95m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.MAGENTA = ''
        self.CYAN = ''
        self.GREEN = ''
        self.YELLOW = ''
        self.RED = ''
        self.ENDC = ''


def InitConfig():
    global configfile

    global opts, args
    (opts, args) = process_args()

    configfile = opts.config
    if not os.path.exists(os.path.dirname(sys.argv[0]) + "/" + configfile):
        print("error: config.ini missing or not readable")
        sys.exit(0)

    if checkAscii(configfile) == 1:
        sys.exit(0)

    parser = configparser.ConfigParser()
    parser.read(configfile)

    if not 'options' in parser.sections():
        print('Error in config file, section "options" is missing')
        sys.exit(0)

    if not '1' in parser.sections():
    #check if there is a config section for at least one nym in config file
        print('Error in config file, section "1" is missing')
        sys.exit(0)


def InitDefaults():

    parser = configparser.ConfigParser()
    parser.read(configfile)

    options = {
        'path'                  :   os.path.dirname(sys.argv[0]),
        'nym-name'              :   parser.get('1', 'name'),
        'nym-email'             :   parser.get('1', 'name') + "@" + parser.get('1', 'server'),
        'nym-server'            :   parser.get('1', 'server'),
        'nym-server-email'      :   "config@" + parser.get('1', 'server'),
        'nym-keyid'             :   False,
        'nym-passphrase'        :   parser.get('1', 'pwd'),
        'final-destination'     :   parser.get('1', 'final_dest'),
        'nym-signsend'          :   parser.get('1', 'signsend'),
        'nym-acksend'           :   parser.get('1', 'acksend'),
        'nym-cryptrecv'         :   parser.get('1', 'cryptrecv'),
        'latency'               :   parser.get('options', 'latency'),
        'remailer-chain'        :   "None",
        'remailer_min_uptime'   :   parser.getint('options', 'remailer_min_uptime'),
        'stats-url'             :   parser.get('options', 'stats_url'),
        'stats-rlist'           :   parser.get('options', 'stats_rlist'),
        'stats-mlist'           :   parser.get('options', 'stats_mlist'),
        'keys-file'             :   parser.get('options', 'keys_file'),
        'message-out'           :   parser.get('options', 'message_out'),
        'mail-to-news'          :   parser.get('options', 'mail_to_news'),
        'keyring'               :   parser.get('options', 'keyring'),
        'subj'                  :   parser.get('options', 'subj'),
        'smtp-sender'           :   parser.get('options', 'smtp_sender'),
        'smtp-server'           :   parser.get('options', 'smtp_server'),
        'smtp-port'             :   parser.getint('options', 'smtp_port'),
        'smtp-password'         :   parser.get('options', 'smtp_password'),
        'mixmaster'             :   parser.get('options', 'mixmaster'),
        'logfile'               :   parser.get('options', 'logfile'),
    }

    # extract nyms data from configfile and store in variables
    nyms = []
    nymdata = []
    keys = []

    for section_name in parser.sections():
        if section_name == 'options':
            pass
        else:
            for name, value in parser.items(section_name):
                nymdata.append(value)
                keys.append(name)
            nyms.append(dict(zip(keys, nymdata)))
            nymdata = []
    nyms = sorted(nyms, key=lambda k: k['id'])
    options['nyms'] = nyms
    return options


def menu(options):
    """ Print menu and wait for user choice """
    global last_message
    os.system(['clear', 'cls'][os.name == 'nt'])
    print(bcolors.GREEN + "----- NYM HELPER -----" + bcolors.ENDC)
    if options['nym-keyid'] is False:
        print(options['nym-email'], options['nym-keyid'])
    else:
        print(bcolors.GREEN + "active nym: %s with keyID: %s" % \
            (options['nym-email'], options['nym-keyid']))
    print(bcolors.GREEN + "final destination for nym: %s" % \
        options['final-destination'] + bcolors.ENDC)
    print(bcolors.GREEN + "choosed remailer chain: %s" % \
        options['remailer-chain'] + bcolors.ENDC)
    if re.match("OK", last_message):
        print(bcolors.YELLOW + "\ndebug: %s\n" % last_message + bcolors.ENDC)
    else:
        print(bcolors.RED + "\ndebug: %s\n" % last_message + bcolors.ENDC)
    #present menu and get selection
    selection = input("Enter\n\
    1 to choose/select active nym\n\
    2 to write a message (as active nym)\n\
    3 to send the message created via local mixmaster\n\
    4 to download fresh remailer stats\n\
    5 to choose remailers chain\n\
    6 to create or update a nym\n\
    8 to create a new secret key\n\
    9 to delete a secret key\n\
    10 to send msg via smtp (NOT ANONYMOUS)\n\
    q to quit\n\n\n")

    #perform actions based on selection above
    if selection == "1":
        askNym()
        menu(options)
    elif selection == "2":
        writeMessage()
        menu(options)
    elif selection == "3":
        sendMixMsg()
        menu(options)
    elif selection == "4":
        download(options['stats-rlist'])
        download(options['stats-mlist'])
        download(options['keys-file'], pgp=True)
        menu(options)
    elif selection == "5":
        chooseChain(remailerList(list='rlist'))
        menu(options)
    elif selection == "6":
        createNym()
        menu(options)
    elif selection == "8":
        print("\nsecret keys in your keyring:")
        listSecKeys()
        print("\n")
        createKey()
        print("key generated, secret keys in your keyring: ")
        listSecKeys()
        pressKey()
        menu(options)
    elif selection == "9":
        sk = defineSecKey()
        fp = sk["fingerprint"]
        deleteKey(fp)
        msg = "key of %s deleted" % sk["uids"][1]
        last_message = msg
        menu(options)
    elif selection == "10":
        sendMsg()
        menu(options)
    elif selection == "q":
        f = open(options['message-out'], "w")
        sys.exit("bye bye")
    else:
        print("wrong selection")
        pressKey()
        menu(options)


def pressKey():
    """ wait user to press RETURN """
    input("press RETURN to continue\n")


def download(file, pgp=False):
    """ Download remailer stats file """
    print("downloading %s....... please wait" % file)
    import urllib.request

    url = options['stats-url'] + file
    global last_message
    try:
        urllib.request.urlretrieve(url, os.path.dirname(sys.argv[0]) + "/" + file)
    except Exception as e:
        print('Error downloading file.')
        print('Reason: ', e)
        pressKey()

    if pgp:
        key_data = open(options['path'] + "/" +
                        options['keys-file'], "r").read()
        import_keys = gpg.import_keys(key_data)
        last_message = "OK - remailer keys downloaded - %s keys imported" % \
            import_keys.count


def askNym(num=0):
    """ set active nym and its options """
    counter = 0
    if num == 0:
        for n in options['nyms']:
            counter += 1
            print("%d  -  %s" % (counter, options['nyms'][counter - 1]['name']
                    + "@" + options['nyms'][counter - 1]['server']))
        while True:
            ns = input("Choose nym you want to use (by num)): ")
            if ns.isdigit():
                i = (int(ns) - 1)
                if int(ns) > counter:
                    print("error, there are only %d choices" % (counter))
                else:
                    options['nym-server'] = options['nyms'][i]['server']
                    options['nym-name'] = options['nyms'][i]['name']
                    options['nym-server-email'] = "config@" + options['nym-server']
                    options['nym-email'] = options['nym-name'] + "@" + options['nym-server']
                    options['nym-passphrase'] = options['nyms'][i]['pwd']
                    options['final-destination'] = options['nyms'][i]['final_dest']
                    options['nym-signsend'] = options['nyms'][i]['signsend']
                    options['nym-acksend'] = options['nyms'][i]['acksend']
                    options['nym-cryptrecv'] = options['nyms'][i]['cryptrecv']
                    break
            else:
                print("Error, you have to type a number")

        if not options['nym-passphrase']:
            askPassphrase()

    # if num parameters is given, it's for a command line usage of this script
    # set only variables needed to send mail from a nym
    else:
        num -= 1
        options['nym-server'] = options['nyms'][num]['server']
        options['nym-name'] = options['nyms'][num]['name']
        options['nym-server-email'] = "config@" + options['nym-server']
        options['nym-email'] = options['nym-name'] + "@" + options['nym-server']
        options['nym-passphrase'] = options['nyms'][num]['pwd']

        if not options['nym-passphrase']:
            print("error, selected nym has not a passphrase set in config file")
            print("you cannot use this in a crontab")
            pressKey()
            askPassphrase()

    selectSecKey(options['nym-email'])

    return


def askPassphrase():
    """ Ask passphrase for nym secret key if not set in config"""
    pwd = getpass.getpass("insert passphrase for nym secret key: ")
    options['nym-passphrase'] = pwd
    return


def validateEmail(email):
    """ check if the recipient is a valid email address"""
    if re.match(r'[\w\-][\w\-\.]*@[\w\-][\w\-\.]+[a-zA-Z]{1,4}', email):
        return True
    else:
        return False


def remailerList(list):
    """ Generate remailer list from stats and return it as a list of dict with
    keys: name,latency-hist,latency,uptime-hist,uptime,options,email,num """
    global last_message

    if list == "rlist":
        remailers = open(options['path'] + "/" +
                         options['stats-rlist'], "r").readlines()
    else:
        remailers = open(options['path'] + "/" +
                         options['stats-mlist'], "r").readlines()

    rmlist = []
    counter = 1
    for line in remailers:
        """ skip first 4 lines in stat file, useless headers """
        if counter < 5:
            counter += 1
            continue
        if len(line) == 1:
            break
        rmlist.append(line)

    keys = ['name', 'latency-hist', 'latency', 'uptime-hist', 'uptime',
            'options', 'email', 'num']
    remail = []
    counter = 0
    for r in rmlist:
        rem = str.split(r)
        rem[5] = rem[5:]
        rem[5] = ''.join(rem[5])
        rem[6:] = ""
        expr = re.compile("\"%s\"" % rem[0])
        remline = [elem for elem in remailers if expr.search(elem)]
        rememail = str.split(remline[0])[2].strip('<>"')

        counter += 1
        rem.append(rememail)
        rem.append(counter)
        remail.append(dict(zip(keys, rem)))

    return remail


def chooseChain(remailerlist, list='rlist'):
    """ Print remailer list and prompt for chain choice """
    print("list of remailers from %s with uptime > %s%%" % \
        (list, options['remailer_min_uptime']))
    print("num   name         uptime   options")
    for r in remailerlist:
        if float(r["uptime"][:-1]) < options['remailer_min_uptime']:
            continue
        print("%-5s %-12s %-8s %s" % (r["num"], r["name"], r["uptime"], r["options"]))
    print("options: D = middleman,  P = post to newsgroups,  U = esub")
    chain = input('Choose remailer to be chained (by num), separated by commas, or write "r" for a random chain (3 remailers): ')
    if chain == "r":
        chooseRandomChain(remailerlist, list)
    else:
        validateChain(remailerlist, chain, list)


def chooseRandomChain(remailerlist, list='rlist'):
    """ Choose a 3 random remailer chain """
    n = len(remailerlist)
    for r in remailerlist:
        if float(r["uptime"][:-1]) < options['remailer_min_uptime']:
            n -= 1

    r1 = str(random.randrange(1, n + 1))
    r2 = str(random.randrange(1, n + 1))
    while r2 == r1:
        r2 = str(random.randrange(1, n + 1))
    r3 = str(random.randrange(1, n + 1))
    while r3 == r2:
        r3 = str(random.randrange(1, n + 1))
    while checkMiddle(remailerlist, str(r3)):
        r3 = str(random.randrange(1, n + 1))

    chain = r1 + "," + r2 + "," + r3
    validateChain(remailerlist, chain, list, random=True)


def validateChain(remailerlist, chain, list, random=False):
    """
    Validate remailer chain, check the availability of public key of remailers
    and that last remailer of chain is not middleman
    """
    global last_message
    global remailer_chain
    global remailer_chain_email
    remailer_chain = []
    remailer_chain_email = []

    comma = re.compile(',')
#    if len(chain) > 1:
#        if not comma.search(chain):
#            print bcolors.RED + "ERROR - remailer chain selections have to be separated by commas" + bcolors.ENDC
#            pressKey()
#            return
    a = chain.split(',')
    for n in a:
        if not n.isdigit():
            print(bcolors.RED + "ERROR - remailer chain selection cannot contains non-numeric chars" + bcolors.ENDC)
            return
        n = int(n)
        n -= 1
        remailer_chain.append(remailerlist[n]["name"])
        remailer_chain_email.append(remailerlist[n]["email"])
        if list == "rlist":
            selectPubKey(remailerlist[n]["email"])

    # check if last remailer is middleman
    if checkMiddle(remailerlist, a[-1]):
        print(bcolors.RED + "ERROR - remailer %s is middleman, it cannot be last remailer in chain" % remailerlist[(int(a[-1]) - 1)]["name"] + bcolors.ENDC)
        pressKey()
        chooseChain(remailerlist, list)

    if checkBrokenChains(remailer_chain):
        if random:
            chooseRandomChain(remailerlist, list)
        else:
            print(checkBrokenChains(remailer_chain))
            pressKey()
            chooseChain(remailerlist, list)

    options['remailer-chain'] = remailer_chain
    last_message = "OK - remailer chain choosed: %s" % remailer_chain


def checkMiddle(remailerlist, nr):
    """ check if a remailer is middleman """
    r = re.compile('D')
    i = int(nr) - 1
    if r.search(remailerlist[i]["options"]):
        return True
    else:
        return False


def checkBrokenChains(remailernamelist):
    """check for broken chains"""
    a = []
    b = []
    in_broken_block = False
    l = open(options['path'] + "/" + options['stats-rlist'], "r").readlines()
    for line in l:
        if "Broken type-I remailer chains" in line:
            in_broken_block = True
        elif in_broken_block:
            elems = line.strip().strip('()').split()
            if len(elems) == 0:
                in_broken_block = False
            else:
                a.append(elems[0])
                b.append(elems[1])

    for n in range(0, len(remailer_chain) - 1):
        for i in range(0, len(a)):
            if remailer_chain[n] == a[i] or a[i] == "*":
                if remailer_chain[n + 1] == b[i] or b[i] == "*":
                    msg = "broken chain: %s - %s" % (a[i], b[i])
                    return msg
    return False


def encrypt(message, recipient, sign=False, passphrase=False, test=False):
    """ Encrypt a message for a recipient """
    global last_message
    encrypted_ascii_data = gpg.encrypt(message, recipient, always_trust=True,
                                    sign=sign, passphrase=passphrase)
    if not test:
        if encrypted_ascii_data.status != "encryption ok":
            print(bcolors.YELLOW + "ERROR" + encrypted_ascii_data.status + \
                " (recipient: %s)" % recipient + bcolors.ENDC)
            last_message = encrypted_ascii_data.status + " (recipient: %s)" % recipient
            menu(options)
    else:
        return encrypted_ascii_data
    return encrypted_ascii_data


def createKey(comment="", key_type="RSA", key_length=4096):
    """ Create a gpg key """
    global last_message
    email = input("insert email address for the key: ")
    name = input("insert real name for the key: ")
    while True:
        pwd = getpass.getpass("insert the password: ")
        pwd2 = getpass.getpass("type again the password: ")
        if pwd == pwd2:
            break
    print("wait, I'm generating the key, move the mouse, press keys....\n")
    input_data = gpg.gen_key_input(name_real=name, name_email=email, passphrase=pwd, key_type=key_type, key_length=key_length)
    key = gpg.gen_key(input_data)
    last_message = "OK - key generated"


def listSecKeys():
    """ Print list of secret keys in keyring"""
    seckeys = gpg.list_keys(secret=True)
    counter = 0
    for k in seckeys:
        counter += 1
        print("%s - %s - %s" % (counter, k["uids"][0], k["keyid"]))
    return seckeys


def selectPubKey(email):
    """ Select the key fingerprint of an email address"""
    global last_message
    pubkeys = gpg.list_keys()
    key = False
    for k in pubkeys:
        counter = 0
        # if keys have multiple uids, scan through all uids
        for u in k["uids"]:
            # stop at first match
            e = k["uids"][counter]
            counter += 1
            expr = re.compile("%s" % email)
            m = expr.search(e)
            """ if match found, select key """
            if m:
                """check if key is valid"""
                if encrypt("test", k["fingerprint"], test=True).status == "encryption ok":
                    key = True
                    last_message = "OK - public key choosed: keyID=%s" % getKeyID(k["fingerprint"])
                    break
                else:
                    last_message = "ERROR - invalid key for recipient: %s" % u
                    key = False
        if key:
            break
    if not key:
        last_message = "ERROR, public key for %s NOT found" % email
        menu(options)
    return k["fingerprint"]


def selectSecKey(email):
    """ Select the key fingerprint of an email address"""
    global last_message
    seckeys = gpg.list_keys(True)
    key = False
    for k in seckeys:
        counter = 0
        """if keys have multiple uids, scan through all uids"""
        for u in k["uids"]:
            """stop at first match"""
            e = k["uids"][counter]
            counter += 1
            expr = re.compile("%s" % email)
            m = expr.search(e)
            """ if match found, select key """
            if m:
                #print "key fingerprint for %s: %s" % (email, k["fingerprint"])
                options['nym-fp'] = k["fingerprint"]
                options['nym-keyid'] = getKeyID(k["fingerprint"])
                key = True
                last_message = "OK - secret key choosed: keyID=%s" % options['nym-keyid']
                break
        if key:
            break
    if not key:
        options['nym-keyid'] = False
        last_message = "ERROR, secret key for %s NOT found" % email
        menu(options)
    return k["fingerprint"]


def getKeyID(fp):
    """ Get keyid from fingeprint """
    # works only with v4 keys
    return fp[-8:]


def exportPubKey(id):
    """ Export public key
    id can be the fingerprint """
    ascii_armored_public_keys = stripVersion(str(gpg.export_keys(id)))
    return ascii_armored_public_keys


def defineSecKey():
    """ Select which secret key to use """
    ls = listSecKeys()
    numkeys = int(len(ls))
    print("there are %s keys" % numkeys)
    if numkeys == 0:
        print("going to generate a new key...")
        createKey()
        listSecKeys()

    while True:
        a = input("enter key number (and press enter): ")
        if a.isdigit():
            a = int(a)
            if a < 1:
                print("you can't enter a key number lower than 1")
                continue
            elif a > numkeys:
                print("there are only %d keys, select key number again" % numkeys)
                continue
            break
    a -= 1
    return ls[a]


def deleteKey(fp):
    """ Delete secret and public key """
    global last_message
    seckey = gpg.list_keys(secret=True)
    str(gpg.delete_keys(fp, True))
    str(gpg.delete_keys(fp))
    last_message = "OK - key with fingerprint %s deleted" % fp


def createNym():
    """ Create or update a nym """
    print("\nFor new nym, add them to the config file first...\n")
    askNym()
    fp = selectSecKey(options['nym-email'])
    pk = exportPubKey(fp)
    print("\nSelect remailer chain for reply block\n")
    chooseChain(remailerList(list='rlist'))
    if not remailer_chain_email:
        return

    ReplyBlock = createReplyBlock(remailer_chain_email)
    RequestMessage = requestMessage(pk, ReplyBlock)
    EncryptedRequestMessage = stripVersion(str(encrypt(RequestMessage, options['nym-server-email'], sign=options['nym-fp'], passphrase=options['nym-passphrase'])))
    f = open(options['message-out'], "w")
    f.write(EncryptedRequestMessage)
    f.close
    global last_message
    last_message = "OK - you can find the message here: %s  - send it to %s \
        through a remailer chain (you can press '3' for this)." % \
        (options['message-out'], options['nym-server-email'])


def createReplyBlock(remailer_chain_email):
    """ Create a reply block """
    r = remailer_chain_email[:]
    r.reverse()
    ub = encrb1(options['final-destination'])
    """ if final dest is a newsgroup, don't use remailer chain"""
    if not validateEmail(options['final-destination']):
        return ub
    eb = stripVersion(str(encrypt(ub, r[0])))
    loops = len(r)
    for i in range(1, len(r)):
        l = i - 1
        ub = encrb2(r[l], eb)
        eb = stripVersion(str(encrypt(ub, r[i])))
    ub = encrb3(r[-1], eb)
    return ub


def encrb1(dest):
    """ Template for reply block (last hop) """
    template = "::\n"
    if validateEmail(options['final-destination']):
        template = template + "Anon-To: %s\n" % dest
        template = template + "Latent-Time: %s\n" % options['latency']
    else:
        template = template + "Anon-To: %s\n" % options['mail-to-news']
        template = template + "Hash-Subject: yes\n"
        template = template + "\n##\n"
        template = template + "Newsgroups: %s\n" % dest
        template = template + "Subject: %s\n" % options['subj']
    return template


def encrb2(dest, msg):
    """ Template for reply block """
    template = "::\n"
    template = template + "Anon-To: %s\n" % dest
    template = template + "Latent-Time: %s\n\n" % options['latency']
    template = template + "::\n"
    template = template + "Encrypted: PGP\n\n"
    template = template + "%s" % msg
    return template


def encrb3(dest, msg):
    """ Template for reply block, last remailer """
    template = "::\n"
    template = template + "Anon-To: %s\n" % dest
    template = template + "Latent-Time: %s\n\n" % options['latency']
    template = template + "::\n"
    template = template + "Encrypted: PGP\n\n"
    template = template + "%s" % msg
    template = template + "\n**"
    return template


def requestMessage(publickey, replyblock):
    """Last step is to add the trimmings to the configuration request.  This
    wraps the Reply-Blocks that in turn contain the Destination-Block."""
    request = "Config:"
    request = request + "\nFrom: " + options['nym-email']
    #request = request + '\nNym-Commands: create? +acksend +cryptrecv +signsend name="'
    request = request + '\nNym-Commands: create? name="'
    request = request + options['nym-name'] + '"'
    if options['nym-cryptrecv'] == "1":
        request = request + " +cryptrecv"
    else:
        request = request + " -cryptrecv"
    if options['nym-acksend'] == "1":
        request = request + " +acksend"
    else:
        request = request + " -acksend"
    if options['nym-signsend'] == "1":
        request = request + " +signsend"
    else:
        request = request + " -signsend"
    request = request + "\nPublic-Key:\n"
    request = request + publickey + "\n"
    request = request + "Reply-Block:\n"
    request = request + str(replyblock)
    return request


def writeSendMessage():
    if not opts.recipient:
        print("error, choose recipient with -r command line parameter")
        sys.exit(-1)
    if not opts.subject:
        print("error, choose subject with -s command line parameter")
        sys.exit(-1)
    if not opts.text:
        print("error, choose text with -t command line parameter")
        sys.exit(-1)

    # set nym options
    askNym(opts.nym)

    etext = opts.text
    msg = "From: %s\n" % options['nym-name']
    if validateEmail(opts.recipient):
        msg = msg + "To: %s\n" % opts.recipient
    else:
        msg = msg + "To: %s\n" % options['mail-to-news']
        msg = msg + "Newsgroups: %s\n" % opts.recipient
    msg = msg + "Subject: %s\n\n" % opts.subject

    msg = msg + etext
    emsg = stripVersion(str(encrypt(msg, options['nym-server-email'],
                                    sign=options['nym-fp'],
                                    passphrase=options['nym-passphrase'])))
    f = open(options['message-out'], 'w')
    f.write(emsg)
    f.close()

    """ send msg via remailer chain through a local mixmaster client """
    to_addr = "send@" + options['nym-server']
    subj = generateHash()
    args = [options['mixmaster'], '--mail', '--to=' + to_addr, '--subject=' + subj]

    if not opts.chain:
        args.append('--chain=*,*')
    else:
        args.append('--chain=' + opts.chain)

    fp = open(options['message-out'], 'rb')
    msg = fp.read()
    fp.close()

    try:
        mix = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = mix.communicate(msg)
        err = err.decode(sys.stdin.encoding)
        if err.find('Error') >= 0:
            raise MixError('Mixmaster process returned the following error: ' + str(err) + '. Sending failed.')
        last_message = "OK, succesfully sent email"
    except OSError:     # usally means that mixmaster could not be executed
        raise MixError('Could not find mixmaster binary.')

    print("task ended, message from nym n.%s to %s sent" % \
        (opts.nym, opts.recipient))
    sys.exit(-1)


def writeMessage():
    """
    Create a message to be sent from nym
    The message has to be sent through a remailer chain
    to send@nym-server-address
    """
    global last_message
    if options['nym-keyid'] is False:
        last_message = "ERROR - select your nym, actual selection HAS NOT a secret key"
        menu(options)
    subject = input("Insert message subject: ")
    text = []
    getmore = True
    prompt = "Write message, use only ascii characters, finish with empty line: \n> "
    while getmore:
        line = input(prompt)
        if len(line) > 0:
            text.append(line)
            prompt = "> "
        else:
            getmore = False
    text = '\n'.join(text)
    text = str(filter(lambda x: x in string.printable, text))

    recipient = input("Insert recipient email address or newsgroup name: ")
    #if encrypt("test",recipient,test=True).status == "encryption ok":
    #    etext = stripVersion(str(encrypt(text,recipient_fp)))
    #    print "message will be encrypted with %s public key" % recipient
    #    pressKey()
    #else:
    #    etext = text
    #    print "msg will not be encrypted, public key for %s not found" % recipient
    #    pressKey()
    etext = text

    msg = "From: %s\n" % options['nym-name']
    if validateEmail(recipient):
        msg = msg + "To: %s\n" % recipient
    else:
        msg = msg + "To: %s\n" % options['mail-to-news']
        msg = msg + "Newsgroups: %s\n" % recipient
    msg = msg + "Subject: %s\n\n" % subject

    msg = msg + etext
    emsg = stripVersion(str(encrypt(msg, options['nym-server-email'],
                            sign=options['nym-fp'],
                            passphrase=options['nym-passphrase'])))
    f = open(options['message-out'], 'w')
    f.write(emsg)
    f.close()
    last_message = "OK - you can find the message here: %s - send it to \
      send@%s though a remailer chain (you can press '3' for this)." % \
      (options['message-out'], options['nym-server'])


def sendMsg():
    """ send msg directly via local or remote smtp """
    global last_message
    import smtplib
    from email.mime.text import MIMEText
    fp = open(options['message-out'], 'rb')
    msg = MIMEText(fp.read(), 'plain', 'utf-8')
    fp.close()

    from_addr = raw_default("Insert sender, or press RETURN to accept default", options['smtp-sender'])
    r = re.compile('config@')
    if r.search(last_message):
        to_addr = raw_default("Insert recipient, or press RETURN to accept default", "config@" + options['nym-server'])
    else:
        to_addr = raw_default("Insert recipient, or press RETURN to accept default", "send@" + options['nym-server'])
    user = options['smtp-sender']
    pwd = options['smtp-password']

    hash = generateHash()
    msg['Subject'] = raw_default("Insert subject, or press RETURN to accept random subject", hash)
    msg['From'] = from_addr
    msg['To'] = to_addr

    s = smtplib.SMTP(options['smtp-server'], options['smtp-port'])
    s.set_debuglevel(0)
    s.ehlo()
    if options['smtp-password']:
        s.starttls()
        s.login(user, pwd)

    try:
        s.sendmail(from_addr, to_addr, msg.as_string())
        last_message = "OK, succesfully sent email"
    except:
        last_message = "Error: unable to send email"
    finally:
        s.quit()


def generateHash():
    # Pseudo random 16 chars string
    hash = ''.join(random.sample(string.ascii_letters + string.digits, 16))
    return hash


class MixError(Exception):
    pass


def sendMixMsg():
    """ send msg via remailer chain through a local mixmaster client """
    global last_message
    r = re.compile('config@')
    if r.search(last_message):
        to_addr = raw_default("Insert recipient, or press RETURN to accept default", "config@" + options['nym-server'])
    else:
        to_addr = raw_default("Insert recipient, or press RETURN to accept default", "send@" + options['nym-server'])
    subj = input("Insert subject and press RETURN: ")
    args = [options['mixmaster'], '--mail', '--to=' + to_addr, '--subject=' + subj]
    # args.append('--header=From: '+options['smtp-sender'])
    print("\nChoose remailer chain used to send the message to %s\n" % to_addr)
    chooseChain(remailerList(list='mlist'), list='mlist')
    args.append('--chain=' + ','.join(remailer_chain))

    fp = open(options['message-out'], 'rb')
    msg = fp.read()
    fp.close()

    try:
        mix = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = mix.communicate(msg)
        err = err.decode(sys.stdin.encoding)
        if err.find('Error') >= 0:
            raise MixError('Mixmaster process returned the following error: ' + str(err) + '. Sending failed.')
        last_message = "OK, succesfully sent email"
    except OSError:    # usally means that mixmaster could not be executed
        raise MixError('Could not find mixmaster binary.')


def raw_default(prompt, dflt=None):
    """ manage defaults for user input """
    if dflt:
        prompt = "%s [%s]: " % (prompt, dflt)
    res = input(prompt)
    if not res and dflt:
        return dflt
    return res


def stripVersion(pgptext):
    """Version strings in PGP blocks can weaken anonymity by partitioning users
    into subsets.  This function replaces the Version string with 'N/A'."""
    newtext = re.sub('(?m)^Version: .*', 'Version: N/A', pgptext)
    return newtext


def checkAscii(file):
    """ Check if the config file is pure ascii """
    import codecs
    bad = 0
    f = codecs.open(file, encoding='ascii')
    lines = open(file).readlines()
    for i in range(0, len(lines)):
        try:
            l = f.readline()
        except:
            num = i + 1
            print('config file problem (%s): line %d contains non-ascii characters, fix it' % (configfile, num))
            bad = 1
            break
    f.close()
    return bad


def process_args():
    """ Process command line arguments """
    from optparse import OptionParser
    use = "nymhelper.py [options] \nexcept for -C, all other options are \
just used to send a message from command line\nfor normal use do not use any options"
    parser = OptionParser(usage=use)
    parser.add_option("-C", "--config", action="store", dest="config", default="config.ini",
                      help="config file to use nym to send the message from.")
    parser.add_option("-n", "--nym", action="store", dest="nym", type="int",
                      help="nym to send the message from.")
    parser.add_option("-s", "--subject", action="store", dest="subject",
                      help="subject for the message to be sent.")
    parser.add_option("-r", "--recipient", action="store", dest="recipient",
                      help="recipient of the message to be sent.")
    parser.add_option("-t", "--text", action="store", dest="text",
                      help="text of the message to be sent.")
    parser.add_option("-c", "--chain", action="store", dest="chain",
                      help="remailer chain to send the message through (example: -c \"remailer1,remailer2\").")

    return parser.parse_args()


def initialChecks():
    """ performs some checks """
    try:
        f = open(options['message-out'], "w")
    except IOError:
        print("Error: you cannot write to %s, fix permission or change \
            setting in config.ini" % options['message-out'])
        sys.exit()
    else:
        f.close()

    try:
        f = open(options['path'] + "/check", "w")
    except:
        print("Error: you cannot write %s, fix permission or launch this script from another directory" % options['path'] + "/" + options['stats-file'])
        sys.exit()
    else:
        f.close()
        os.remove(options['path'] + "/check")

    try:
        f = open(options['path'] + "/" + options['stats-rlist'], "r")
    except:
        download(options['stats-rlist'])
    else:
        f.close()

    try:
        f = open(options['path'] + "/" + options['stats-mlist'], "r")
    except:
        download(options['stats-mlist'])
    else:
        f.close()

    try:
        global gpg
        gpg = gnupg.GPG(gnupghome=options['keyring'])
    except:
        print("Error, problems with your keyring directory: %s, check settings in config.ini" % options['keyring'])
        sys.exit(0)

    selectSecKey(options['nym-email'])
    global last_message
    last_message = "OK, initial check passed"


#import logging
#LOG_FILENAME = options['logfile']
#try:
#    logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
#except IOError:
#    print "Error: you cannot write to %s, fix permission or change setting in config.ini" % options['logfile']
#    sys.exit()

InitConfig()

options = InitDefaults()

initialChecks()

try:
    if opts.nym:
        writeSendMessage()
    else:
        menu(options)
except KeyboardInterrupt:
    print("program terminated")
except SystemExit:
    print("program terminated, bye")
except:
    print(bcolors.RED + "\nAn unhandled exception occured, here's the traceback!\n" + bcolors.ENDC)
    traceback.print_exc()
    print(bcolors.RED + "\nReport this to putro@autistici.org" + bcolors.ENDC)
    sys.exit()
